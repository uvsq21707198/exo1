/**
 * Created by m.najafi on 10/18/2017.
 */
public class ChaineCryptee {


    private String store;
    private int decalage;

    private ChaineCryptee(String enclair, int decalage) {
        this.store = decaleString(enclair, decalage);
        this.decalage = decalage;

    }
    public static ChaineCryptee deCryptee(String messageCrypte , int decalage){
        ChaineCryptee c = new ChaineCryptee(null, decalage);
        c.store = messageCrypte;
        return c;
    }

    public static ChaineCryptee deEnClair(String messageClaire, int decalage){
        return new ChaineCryptee(messageClaire, decalage);
    }

    public String crypte() {
        return store;

    }

    public String decrypte() {
        return decaleString(store, -decalage);

    }

    private String decaleString(String str, int decalage){
        int i;
        char c;
        String strDecalee = "";
        if (str == null) {
            return null;
        }
        for (i = 0; i < str.length(); i++) {
            c = decaleCaractere(str.charAt(i), decalage);
            strDecalee += c;
        }
        return strDecalee;
    }

    private char decaleCaractere(char c, int decalage) {
        return (c < 'A' || c > 'Z') ? c : (char) ((((c - 'A' + decalage) % 26) + 26) % 26 + 'A');
    }


}


